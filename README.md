# Sales Grids

A Magento CE 1.9+ module allowing to customize order, invoice, shipment and credit memo grid.

## Installation Instructions

1. Backup your store database and web directory.
2. Using your favorite FTP client upload "app" directory to your store root.
3. Clear the store cache under var/cache.
