<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Slawomir Iwanczuk
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

class Mage_SalesGrids_Model_Resource_Order
    extends Mage_Sales_Model_Resource_Order
{
    protected function _initVirtualGridColumns()
    {
        parent::_initVirtualGridColumns();

        $this->addVirtualGridColumn(
            'product_sku',
            'sales/order_item',
            array('entity_id' => 'order_id'),
            'GROUP_CONCAT(DISTINCT {{table}}.sku ORDER BY {{table}}.item_id ASC SEPARATOR "\n")'
        );

        $this->addVirtualGridColumn(
            'product_name',
            'sales/order_item',
            array('entity_id' => 'order_id'),
            'GROUP_CONCAT(DISTINCT {{table}}.name ORDER BY {{table}}.item_id ASC SEPARATOR "\n")'
        );

        $this->addVirtualGridColumn(
            'payment_method',
            'sales/order_payment',
            array('entity_id' => 'parent_id'),
            '{{table}}.method'
        );

        return $this;
    }

    public function getUpdateGridRecordsSelect($ids, &$flatColumnsToSelect, $gridColumns = null)
    {
        $select = parent::getUpdateGridRecordsSelect($ids, $flatColumnsToSelect, $gridColumns);

        $select->group('main_table.' . $this->getIdFieldName());

        return $select;
    }

    public function updateOnRelatedRecordChanged($field, $entityId)
    {
        $adapter = $this->_getWriteAdapter();
        $column = array();

        $select = $adapter->select()
            ->from(array('main_table' => $this->getMainTable()), $column)
            ->where('main_table.' . $field .' = ?', $entityId)
            ->group('main_table.' . $this->getIdFieldName());

        $this->joinVirtualGridColumnsToSelect('main_table', $select, $column);
        $fieldsToUpdate = $adapter->fetchRow($select);

        if ($fieldsToUpdate) {
            return $adapter->update(
                $this->getGridTable(),
                $fieldsToUpdate,
                $adapter->quoteInto($this->getGridTable() . '.' . $field . ' = ?', $entityId)
            );
        }

        return $this;
    }
}
